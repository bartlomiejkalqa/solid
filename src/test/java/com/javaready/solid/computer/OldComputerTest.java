package com.javaready.solid.computer;

import org.junit.jupiter.api.Test;

class OldComputerTest {

    @Test
    public void testDependencyInversion() {
        final OldComputer oldComputer = new OldComputer(new WiredKeyboard(), new AsusMonitor());

        oldComputer.start();
    }

    @Test
    public void testDependencyInversion2() {
        final OldComputer oldComputer = new OldComputer(new WirelessKeyboard(), new AsusMonitor());

        oldComputer.start();
    }
}