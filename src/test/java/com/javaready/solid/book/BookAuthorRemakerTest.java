package com.javaready.solid.book;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BookAuthorRemakerTest {

    @Test
    public void shouldAddPrefixAndPostfixToAuthor() {
        // given
        BookPostfixAppender bookPostfixAppender = new BookPostfixAppender();
        BookPrefixAppender bookPrefixAppender = new BookPrefixAppender();
        BookAuthorRemaker bookAuthorRemaker = new BookAuthorRemaker(bookPostfixAppender, bookPrefixAppender);
        // when
        String authorWithPrefixAndPostfix = bookAuthorRemaker.addPrefixAndPostfixToText("mr", "java", "ready");

        //then
        assertThat(authorWithPrefixAndPostfix).isEqualTo("mr java ready");
    }
}