package com.javaready.solid.book;

import com.javaready.solid.book.BookPostfixAppender;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BookAuthorPostfixAppenderTest {

    @Test
    public void shouldCorrectlyAppendPostfixToAuthor() {
        // given
        BookPostfixAppender bookPostfixAppender = new BookPostfixAppender();
        // when
        String addedPostfixText = bookPostfixAppender.addPostfixToGivenText("master", "jr");
        // then
        assertThat(addedPostfixText).isEqualTo("master jr");
    }
}