package com.javaready.solid.book;

import com.javaready.solid.book.BookPrefixAppender;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class BookAuthorPrefixAppenderTest {

    @Test
    public void shouldCorrectlyAppendPrefixToAuthor() {
        // given
        BookPrefixAppender book = new BookPrefixAppender();

        String authorWithPrefix = book.addPrefixToBookAuthor("mr", "author");

        assertThat(authorWithPrefix).isEqualTo("mr author");
    }
}