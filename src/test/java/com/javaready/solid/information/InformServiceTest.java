package com.javaready.solid.information;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class InformServiceTest {

    @Test
    public void testsForMe() {
        Informable informable;
        EmailSender emailSender = new EmailSender();
        GaduGaduSender gaduGaduSender = new GaduGaduSender();

        informable = emailSender;
        informable.sendMessages(Arrays.asList("czesc"));
        ((ViewCount) emailSender).getSeenCount();

        informable = gaduGaduSender;
        informable.sendMessages(Arrays.asList("czesc"));
    }
}