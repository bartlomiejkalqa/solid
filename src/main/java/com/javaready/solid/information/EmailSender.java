package com.javaready.solid.information;

import java.util.List;

class EmailSender implements Informable, ViewCount {

    @Override
    public void sendMessages(List<String> messages) {
        // wyslij wiadomosci
    }

    @Override
    public int getSeenCount() {
        return 0;
    }
}
