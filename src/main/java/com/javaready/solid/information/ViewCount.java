package com.javaready.solid.information;

public interface ViewCount {

    int getSeenCount();
}
