package com.javaready.solid.information;

import java.util.List;

public interface Informable {

    void sendMessages(List<String> messages);

}
