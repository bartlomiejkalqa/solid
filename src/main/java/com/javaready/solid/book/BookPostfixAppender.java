package com.javaready.solid.book;

class BookPostfixAppender {

    public String addPostfixToGivenText(String text, String postfix) {
        return text + " " + postfix;
    }
}
