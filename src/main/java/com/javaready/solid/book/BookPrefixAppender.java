package com.javaready.solid.book;

class BookPrefixAppender {

    public String addPrefixToBookAuthor(String prefix, String author) {
        return prefix + " " + author;
    }
}
