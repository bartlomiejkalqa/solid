package com.javaready.solid.book;

class BookAuthorRemaker {

    BookPostfixAppender bookPostfixAppender;
    BookPrefixAppender bookPrefixAppender;

    public BookAuthorRemaker(BookPostfixAppender bookPostfixAppender, BookPrefixAppender bookPrefixAppender) {
        this.bookPostfixAppender = bookPostfixAppender;
        this.bookPrefixAppender = bookPrefixAppender;
    }

    public String addPrefixAndPostfixToText(String prefix, String author, String postfix) {
        final String bookAuthorWithPrefix = bookPrefixAppender.addPrefixToBookAuthor(prefix, author);
        return bookPostfixAppender.addPostfixToGivenText(bookAuthorWithPrefix, postfix);
    }
}
