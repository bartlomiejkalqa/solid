package com.javaready.solid.computer;

class OldComputer {

    private final Keyboard keyboard;
    private final Monitor asusMonitor;

    public OldComputer(Keyboard keyboard, Monitor monitor) {
        this.keyboard = keyboard;
        this.asusMonitor = monitor;
    }

    public void start() {
        // start computer
        System.out.println("started!");
    }
}
